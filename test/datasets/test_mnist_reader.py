import numpy as np
import os
import pytest
from nwdata import StaticBatchedReader
from nwdata.datasets import MNISTReader
from pathlib import Path

try:
	# This path must be supplied manually in order to pass these tests
	MNIST_DATASET_PATH = os.environ["MNIST_DATASET_PATH"]
	pytestmark = pytest.mark.skipif(False, reason="Dataset path not found.")
	trainPath = Path(MNIST_DATASET_PATH) / "train.h5"
	testPath = Path(MNIST_DATASET_PATH) / "test.h5"
except Exception:
	pytestmark = pytest.mark.skip("MNIST Dataset path must be set.", allow_module_level=True)

class TestMNISTReader:
	@pytestmark
	def test_mnist_construct_1(self):
		reader = MNISTReader(trainPath)
		assert reader.datasetFormat.dataBuckets == {"data" : ["images"], "labels" : ["labels"]}

	@pytestmark
	def test_mnist_construct_2(self):
		trainReader = MNISTReader(trainPath)
		testReader = MNISTReader(testPath)
		assert trainReader.getDataset()["images"].shape == (60000, 28, 28)
		assert testReader.getDataset()["images"].shape == (10000, 28, 28)

	@pytestmark
	def test_getNumData_1(self):
		trainReader = MNISTReader(trainPath)
		testReader = MNISTReader(testPath)
		assert len(trainReader) == 60000
		assert len(testReader) == 10000

	@pytestmark
	def test_getNumIterations_1(self):
		trainReader = MNISTReader(trainPath)
		testReader = MNISTReader(testPath)
		batches = [1, 2, 7, 10, 23, 100, 444, 9999, 10000]
		trainExpected = [60000, 30000, 8572, 6000, 2609, 600, 136, 7, 6]
		testExpected = [10000, 5000, 1429, 1000, 435, 100, 23, 2, 1]
		for i in range(len(batches)):
			trainReader = StaticBatchedReader(trainReader, batches[i])
			testReader = StaticBatchedReader(testReader, batches[i])
			g = trainReader.iterate()
			N = len(g)
			assert trainExpected[i] == len(g), "%d vs %d" % (trainExpected[i], N)
			assert testExpected[i] == len(testReader.iterate())

	@pytestmark
	def test_iterate_1(self):
		MB = np.random.randint(1, 200)
		reader = StaticBatchedReader(MNISTReader(trainPath), MB)
		generator = reader.iterateForever()
		assert not generator is None
		items = next(generator)
		assert not items is None
		assert len(items["data"]["images"]) == MB

		rgb, labels = items["data"]["images"], items["labels"]
		assert rgb.shape == (MB, 28, 28)
		assert labels.shape == (MB, 10)

	@pytestmark
	def test_iterate_2(self):
		reader = StaticBatchedReader(MNISTReader(trainPath), 30)
		generator = reader.iterateOneEpoch()
		for _ in range(len(generator)):
			_ = next(generator)
		try:
			_ = next(generator)
			assert False
		except StopIteration:
			pass

	@pytestmark
	def test_iterate_3(self):
		reader = StaticBatchedReader(MNISTReader(trainPath), 30)
		generator = reader.iterate()
		firstItem = next(generator)
		firstRgb, firstLabels = firstItem["data"]["images"], firstItem["labels"]
		for _ in range(len(generator) - 1):
			_ = next(generator)
		firstItem2 = next(generator)
		firstRgbEpoch2, firstLabelsEpoch2 = firstItem2["data"]["images"], firstItem2["labels"]

		assert len(firstRgb) == len(firstRgbEpoch2)
		assert np.allclose(firstRgb, firstRgbEpoch2)
		assert np.allclose(firstLabels, firstLabelsEpoch2)

	@pytestmark
	def test_normalization_1(self):
		reader = StaticBatchedReader(MNISTReader(trainPath, normalization="none"), 30)
		generator = reader.iterate()
		firstRGBs = next(generator)["data"]["images"]

		assert firstRGBs.dtype == np.uint8 and firstRGBs.min() == 0 and firstRGBs.max() == 255

	@pytestmark
	def test_normalization_2(self):
		reader = StaticBatchedReader(MNISTReader(trainPath, normalization="min_max_0_1"), 30)
		generator = reader.iterate()
		firstRGBs = next(generator)["data"]["images"]

		assert firstRGBs.dtype == np.float32 and firstRGBs.min() == 0 and firstRGBs.max() == 1

def main():
	pass
	# TestMNISTReader().test_mnist_construct_1()
	# TestMNISTReader().test_mnist_construct_2()
	# TestMNISTReader().test_getNumData_1()
	TestMNISTReader().test_getNumIterations_1()
	# TestMNISTReader().test_iterate_1()
	# TestMNISTReader().test_iterate_2()
	# TestMNISTReader().test_iterate_3()
	# TestMNISTReader().test_normalization_1()
	# TestMNISTReader().test_normalization_2()

if __name__ == "__main__":
	main()