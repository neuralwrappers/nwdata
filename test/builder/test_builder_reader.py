import sys
from nwdata import BuilderReader
from nwutils.data_structures import deepCheckEqual
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parents[1]))
from test_reader import DummyDataset
from test_batched_reader import DummyBatchedDataset

class TestBuilderReader:
	def test_constructor_1(self):
		reader = BuilderReader(DummyDataset())
		assert not reader is None

	def test_iterate_1(self):
		reader = BuilderReader(DummyDataset())
		try:
			g = reader.iterate()
			assert not g is None
		except Exception:
			assert False

	def test_iterate_2(self):
		reader = BuilderReader(DummyDataset())
		g = reader.iterate()
		for i in range(len(reader)):
			assert deepCheckEqual(next(g), reader[i])

class TestBuilderReaderBatched:
	def test_constructor_1(self):
		reader = BuilderReader(DummyBatchedDataset())
		assert not reader is None

	def test_iterate_1(self):
		reader = BuilderReader(DummyBatchedDataset())
		try:
			g = reader.iterate()
			assert not g is None
		except Exception:
			assert False

	def test_iterate_2(self):
		reader = BuilderReader(DummyBatchedDataset())
		g = reader.iterate()
		for i in range(len(reader)):
			assert deepCheckEqual(next(g), reader[i])
