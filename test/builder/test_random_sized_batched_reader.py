import sys
import os
import numpy as np
from overrides import overrides
from typing import Tuple, List, Any
from nwdata import RandomSizedBatchedReader
from nwutils.batched import getBatchLens
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parents[1]))
from test_batched_reader import DummyBatchedDataset

class TestRandomSizedBatchedReader:
	def test_constructor_1(self):
		reader = RandomSizedBatchedReader(DummyBatchedDataset())
		assert not reader is None

	def test_getBatchedItem_1(self):
		reader = RandomSizedBatchedReader(DummyBatchedDataset())
		g = reader.iterate()
		item = g[0]
		rgb = item["data"]["rgb"]
		B = getBatchLens(reader.batches)[0]
		assert rgb.shape[0] == B
		assert np.abs(rgb - reader.baseReader.dataset[0:B]).sum() < 1e-5

	def test_getBatchItem_1(self):
		reader = RandomSizedBatchedReader(DummyBatchedDataset())
		g = reader.iterate()
		batches = reader.batches
		n = len(g)
		for j in range(100):
			index = batches[j % n]
			batchItem = g[j % n]
			rgb = batchItem["data"]["rgb"]
			assert rgb.shape[0] == getBatchLens(batches)[j % n], "%d vs %d" % (rgb.shape[0], batches[j % n])
			assert np.abs(rgb - reader.baseReader.dataset[index.start : index.stop]).sum() < 1e-5

	def test_iterateForever_1(self):
		reader = RandomSizedBatchedReader(DummyBatchedDataset())
		generator = reader.iterateForever(maxPrefetch=0)
		k = 0
		for j, batchItem in enumerate(generator):
			if k == 0 or k % n == 0:
				n = len(generator)
				currentBatch = reader.batches
				k = 0

			rgb = batchItem["data"]["rgb"]
			print("j=%d. batches: %s. rgb:%s" % (j, currentBatch, rgb.shape))
			index = currentBatch[k % n]

			assert len(rgb) == getBatchLens(currentBatch)[k % n]
			assert np.abs(rgb - reader.baseReader.dataset[index.start : index.stop]).sum() < 1e-5

			k += 1
			if j == 100:
				break

	# Two readers going side by side 1 random epoch
	def test_iterate_2(self):
		reader1 = RandomSizedBatchedReader(DummyBatchedDataset(N=100, seed=13), seed=41)
		reader2 = RandomSizedBatchedReader(DummyBatchedDataset(N=100, seed=13), seed=42)
		g1, g2 = reader1.iterate(), reader2.iterate()
		n1, n2 = len(g1), len(g2)

		i1, i2 = 0, 0
		leftover1, leftover2 = np.zeros((0, 3)), np.zeros((0, 3))
		for i in range(min(n1, n2)):
			item1 = next(g1)
			item2 = next(g2)
			rgb1 = item1["data"]["rgb"]
			rgb2 = item2["data"]["rgb"]
			b1 = len(rgb1)
			b2 = len(rgb2)

			rgb1 = np.concatenate([leftover1, rgb1])
			rgb2 = np.concatenate([leftover2, rgb2])
			Min = min(len(rgb1), len(rgb2))
			assert np.abs(rgb1[0 : Min] - rgb2[0 : Min]).sum() < 1e-5

			leftover1 = rgb1[Min :]
			leftover2 = rgb2[Min :]
			i1 += b1
			i2 += b2

		i += 1
		for j in range(i, n1):
			item1 = next(g1)
			rgb1 = item1["data"]["rgb"]
			b1 = len(rgb1)
			assert np.abs(rgb1 - leftover2[0 : b1]).sum() < 1e-5
			leftover2 = leftover2[b1 :]

		for j in range(i, n2):
			item2 = next(g2)
			rgb2 = item2["data"]["rgb"]
			b2 = len(rgb2)
			assert np.abs(rgb2 - leftover1[0 : b2]).sum() < 1e-5
			leftover1 = leftover1[b2 :]
		assert len(leftover1) == 0
		assert len(leftover2) == 0

def main():
	TestRandomSizedBatchedReader().test_iterate_2()

if __name__ == "__main__":
	main()