import numpy as np
import sys
from overrides import overrides
from typing import Tuple, List, Any
from nwdata import Reader, MergeBatchedReader, StaticBatchedReader, BuilderReader, RandomSizedBatchedReader
from nwutils.batched import batchIndexFromBatchSizes, getBatchLens
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parents[1]))
from test_reader import DummyDataset
from test_batched_reader import DummyBatchedDataset

def mergeItems(items:List):
	rgbs = np.stack([item["data"]["rgb"] for item in items], axis=0)
	classes = len(items) * [0]
	item = {"data": {"rgb" : rgbs}, "labels" : {"class" : classes}}
	return item

def batchesFn() -> List[int]:
	# batchSizes = [4, 1, 2, 3], so batch[0] has a size of 4, batch[2] a size of 2 etc.
	batchSizes = np.array([4, 1, 2, 3], dtype=np.int32)
	batches = batchIndexFromBatchSizes(batchSizes)
	return batches

class TestMergeBatchedReader:
	def test_constructor_1(self):
		reader = MergeBatchedReader(DummyDataset(), mergeItems, batchesFn)
		assert not reader is None

	def test_constructor_2(self):
		try:
			reader = MergeBatchedReader(DummyBatchedDataset(), mergeItems, batchesFn)
			assert False
		except Exception:
			pass

	def test_constructor_3(self):
		reader = MergeBatchedReader(DummyDataset(), mergeItems, batchesFn)
		assert not reader is None
		try:
			reader = MergeBatchedReader(CachedDataset(DummyBatchedDataset(), cache=None), batchesFn)
			assert False
		except Exception:
			pass

	def test_getBatchItem_1(self):
		reader = MergeBatchedReader(DummyDataset(), mergeItems, batchesFn)
		g = reader.iterate()
		item = g[0]
		rgb = item["data"]["rgb"]
		assert rgb.shape[0] == 4
		assert np.abs(rgb - reader.dataset[0:4]).sum() < 1e-5

	def test_getBatchItem_2(self):
		reader = MergeBatchedReader(DummyDataset(), mergeItems, batchesFn)
		g = reader.iterate()
		batches = reader.getBatches()
		n = len(batches)
		for j in range(100):
			index = batches[j % n]
			batchItem = g[j % n]
			rgb = batchItem["data"]["rgb"]
			assert np.abs(rgb - reader.dataset[index.start : index.stop]).sum() < 1e-5

	def test_mergeItems_1(self):
		reader = MergeBatchedReader(DummyDataset(), mergeItems, batchesFn)
		gBase = reader.baseReader.iterate()
		item1 = gBase[0]
		item2 = gBase[1]
		itemMerged = mergeItems([item1, item2])
		rgb1 = item1["data"]["rgb"]
		rgb2 = item2["data"]["rgb"]
		rgbMerged = itemMerged["data"]["rgb"]
		assert np.abs(rgb1 - rgbMerged[0]).sum() < 1e-5
		assert np.abs(rgb2 - rgbMerged[1]).sum() < 1e-5

	def test_iterateOneEpoch_1(self):
		reader = MergeBatchedReader(DummyDataset(), mergeItems, batchesFn)
		generator = reader.iterateOneEpoch()
		batches = reader.getBatches()
		batchLens = getBatchLens(batches)
		for i, item in enumerate(generator):
			rgb = item["data"]["rgb"]
			assert len(rgb) == batchLens[i]
			assert len(rgb) == (batches[i].stop - batches[i].start)
		assert i == len(generator) - 1

	def test_iterateOneEpoch_2(self):
		a = DummyDataset()
		b = MergeBatchedReader(DummyDataset(), mergeItems, batchesFn)
		reader = StaticBatchedReader(b, 4)
		batches = reader.getBatches()
		assert getBatchLens(batches) == [4, 4, 2]
		item = next(reader.iterate())
		rgb = item["data"]["rgb"]
		assert len(rgb) == 4

	def test_iterateOneEpoch_3(self):
		reader = DummyDataset()
		reader2 = BuilderReader(reader)
		reader3 = MergeBatchedReader(reader2, mergeItems, batchesFn)

		generator = reader3.iterateOneEpoch()
		batches = reader3.getBatches()
		batchLens = getBatchLens(batches)
		for i, item in enumerate(generator):
			rgb = item["data"]["rgb"]
			assert len(rgb) == batchLens[i]
			assert len(rgb) == (batches[i].stop - batches[i].start)
		assert i == len(generator) - 1

	def test_iterateOneEpoch_4(self):
		reader = RandomSizedBatchedReader(MergeBatchedReader(DummyDataset(), mergeItems, batchesFn), seed=666)
		generator = reader.iterateOneEpoch()
		batches = reader.batches
		for i, item in enumerate(generator):
			rgb = item["data"]["rgb"]
			assert len(rgb) == (batches[i].stop - batches[i].start)
			assert len(rgb) == getBatchLens(batches)[i]
		assert i == len(generator) - 1

class TestRegression:
	def test_regression_1(self):
		B = 4
		N = 10

		try:
			baseReader = DummyDataset(N=10)
			reader = BuilderReader(baseReader)
			reader = StaticBatchedReader(MergeBatchedReader(reader, mergeFn=mergeItems), B)
		except Exception:
			assert False

		try:
			baseReader = StaticBatchedReader(MergeBatchedReader(baseReader, mergeFn=mergeItems), B)
			reader = BuilderReader(baseReader)
			reader = StaticBatchedReader(MergeBatchedReader(reader, mergeFn=mergeItems), B)
			assert False
		except Exception:
			pass

def main():
	# TestMergeBatchedReader().test_getBatchItem_1()
	# TestRegression().test_regression_1()
	TestMergeBatchedReader().test_iterateOneEpoch_4()
	# TestBatchedDataset().test_mergeItems_1()
	# TestBatchedDataset().test_splitItems_1()
	# TestBatchedDataset().test_mergeSplit_1()

if __name__ == "__main__":
	main()