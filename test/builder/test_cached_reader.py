import sys
import os
import numpy as np
import simple_caching
from overrides import overrides
from nwutils.data_structures import deepCheckEqual
from nwdata import CachedReader, StaticBatchedReader, RandomIndexReader
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parents[1]))
from test_reader import DummyDataset
from test_batched_reader import DummyBatchedDataset

class BatchedReader(DummyBatchedDataset):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.isCacheable = True

class Reader(DummyDataset):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.isCacheable = True

class TestCachedReader:
	def test_constructor_1(self):
		reader = CachedReader(Reader(), cache=simple_caching.DictMemory())
		assert not reader is None

	def test_getItem_1(self):
		reader = CachedReader(Reader(), cache=simple_caching.DictMemory(), buildCache=False)
		index = 0
		g = reader.iterate()
		assert reader.cache.check(reader.cacheKey(index)) == False
		item = g[index]
		rgb = item["data"]["rgb"]
		assert reader.cache.check(reader.cacheKey(index)) == True
		itemCache = g[index]
		rgbCache = itemCache["data"]["rgb"]
		assert np.abs(rgb - rgbCache).sum() < 1e-5

	def test_getItem_2(self):
		reader = CachedReader(Reader(), cache=simple_caching.DictMemory(), buildCache=True)
		index = 0
		g = reader.iterate()
		assert reader.cache.check(reader.cacheKey(index)) == True
		item = g[index]
		rgb = item["data"]["rgb"]
		assert reader.cache.check(reader.cacheKey(index)) == True
		itemCache = g[index]
		rgbCache = itemCache["data"]["rgb"]
		assert np.abs(rgb - rgbCache).sum() < 1e-5

	def test_iterateOneEpoch_1(self):
		reader = CachedReader(Reader(), cache=simple_caching.DictMemory(), buildCache=False)
		generator = reader.iterateOneEpoch()
		rgbs = []
		for i in range(len(generator)):
			assert reader.cache.check(reader.cacheKey(i)) == False
			item = next(generator)
			rgb = item["data"]["rgb"]
			rgbs.append(rgb)

		generator = reader.iterateOneEpoch()
		for i in range(len(generator)):
			assert reader.cache.check(reader.cacheKey(i)) == True
			item = next(generator)
			rgb = item["data"]["rgb"]
			assert np.abs(rgbs[i] - rgb).sum() < 1e-5

	def test_iterateForever_1(self):
		reader = CachedReader(Reader(), cache=simple_caching.DictMemory(), buildCache=False)
		generator = reader.iterateForever()

		rgbs = []
		n = len(generator)
		i = 0
		while True:
			if i == 2 * n:
				break

			if i < n:
				assert reader.cache.check(reader.cacheKey(i)) == False
				item = next(generator)
				rgb = item["data"]["rgb"]
				rgbs.append(rgb)
			else:
				assert reader.cache.check(reader.cacheKey(i - n)) == True
				item = next(generator)
				rgb = item["data"]["rgb"]
				assert np.abs(rgb - rgbs[i - n]).sum() < 1e-5
			i += 1

	def test_dirty_1(self):
		cache = simple_caching.DictMemory()

		baseReader = RandomIndexReader(Reader(N=100))
		baseReader.isCacheable = True
		reader1 = CachedReader(baseReader, cache=cache, buildCache=True)
		
		gBase = baseReader.iterateForever()
		g1 = reader1.iterateForever()

		resBase, res1, res2 = [], [], []
		for i in range(len(gBase)):
			resBase.append(next(gBase))
			res1.append(next(g1))
		assert deepCheckEqual(resBase, res1)
		
		# Then, rebuild the cache, it should be dirty, thus a new roll should've been generated
		newBase = RandomIndexReader(baseReader, seed=100)
		newBase.isCacheable = True
		newReader = CachedReader(newBase, cache=cache, buildCache=True)
		gNew = newReader.iterate()
		resNew = []
		for i in range(len(newReader)):
			resNew.append(next(gNew))
		assert not deepCheckEqual(res1, resNew)

class TestCachedReaderBatched:
	def test_constructor_1(self):
		reader = BatchedReader()
		reader = CachedReader(reader, cache=simple_caching.DictMemory())
		assert not reader is None

	def test_getBatchItem_1(self):
		reader = CachedReader(BatchedReader(), cache=simple_caching.DictMemory(), buildCache=False)
		index = 0
		g = reader.iterate()
		assert reader.cache.check(reader.cacheKey(index)) == False
		item = g[index]
		rgb = item["data"]["rgb"]
		assert reader.cache.check(reader.cacheKey(index)) == True
		itemCache = g[index]
		rgbCache = itemCache["data"]["rgb"]
		assert np.abs(rgb - rgbCache).sum() < 1e-5

	def test_getBatchItem_2(self):
		reader = CachedReader(BatchedReader(), cache=simple_caching.DictMemory(), buildCache=True)
		index = 0
		g = reader.iterate()
		assert reader.cache.check(reader.cacheKey(index)) == True
		item = g[index]
		rgb = item["data"]["rgb"]
		assert reader.cache.check(reader.cacheKey(index)) == True
		itemCache = g[index]
		rgbCache = itemCache["data"]["rgb"]
		assert np.abs(rgb - rgbCache).sum() < 1e-5

	def test_iterateOneEpoch_1(self):
		reader = CachedReader(BatchedReader(), cache=simple_caching.DictMemory(), buildCache=False)
		generator = reader.iterateOneEpoch()
		rgbs = []
		for i in range(len(generator)):
			assert reader.cache.check(reader.cacheKey(i)) == False
			item = next(generator)
			rgb = item["data"]["rgb"]
			rgbs.append(rgb)

		generator = reader.iterateOneEpoch()
		for i in range(len(generator)):
			assert reader.cache.check(reader.cacheKey(i)) == True
			item = next(generator)
			rgb = item["data"]["rgb"]
			assert np.abs(rgbs[i] - rgb).sum() < 1e-5

	def test_iterateOneEpoch_2(self):
		baseReader = BatchedReader(N=10)
		reader = CachedReader(StaticBatchedReader(baseReader, 3), cache=simple_caching.DictMemory(), \
			buildCache=False)
		generator = reader.iterateOneEpoch()
		rgbs = []
		for i in range(len(generator)):
			ix = i
			assert reader.cache.check(reader.cacheKey(ix)) == False
			item = next(generator)
			rgb = item["data"]["rgb"]
			rgbs.append(rgb)

		generator1 = reader.iterateOneEpoch()
		assert len(generator) == len(generator1)
		for i in range(len(generator1)):
			assert reader.cache.check(reader.cacheKey(i)) == True
			item = next(generator1)
			rgb = item["data"]["rgb"]
			assert np.abs(rgbs[i] - rgb).sum() < 1e-5

		reader2 = CachedReader(StaticBatchedReader(baseReader, 3), cache=simple_caching.DictMemory(), \
			buildCache=False)
		generator2 = reader2.iterateOneEpoch()
		assert len(generator) == len(generator2)
		for i in range(len(generator2)):
			assert reader.cache.check(reader2.cacheKey(i)) == True
			item = next(generator2)
			rgb = item["data"]["rgb"]
			assert np.abs(rgbs[i] - rgb).sum() < 1e-5

		reader3 = CachedReader(StaticBatchedReader(baseReader, 4), cache=simple_caching.DictMemory(), \
			buildCache=False)
		generator3 = reader3.iterateOneEpoch()
		assert len(generator) != len(generator3)
		for i in range(len(generator3)):
			k = reader3.cacheKey(i)
			chk = reader3.cache.check(k)
			assert chk == False
			item = next(generator3)
			rgb = item["data"]["rgb"]
			assert len(rgbs[i]) != len(rgb)

		reader4 = CachedReader(StaticBatchedReader(baseReader, 4), cache=reader3.cache, buildCache=False)
		generator4 = reader4.iterateOneEpoch()
		assert len(generator3) == len(generator4)
		for i in range(len(generator4)):
			assert reader4.cache.check(reader4.cacheKey(i)) == True

	def test_iterateForever_1(self):
		reader = CachedReader(BatchedReader(), cache=simple_caching.DictMemory(), buildCache=False)
		generator = reader.iterateForever()
		batches = reader.batches
		rgbs = []
		n = len(generator)
		i = 0
		while True:
			if i == 2 * n:
				break

			if i < n:
				assert reader.cache.check(reader.cacheKey(i)) == False
				item = next(generator)
				rgb = item["data"]["rgb"]
				rgbs.append(rgb)
			else:
				assert reader.cache.check(reader.cacheKey(i - n)) == True
				item = next(generator)
				rgb = item["data"]["rgb"]
				assert np.abs(rgb - rgbs[i - n]).sum() < 1e-5
			i += 1

	def test_dirty_1(self):
		cache = simple_caching.DictMemory()

		baseReader = RandomIndexReader(BatchedReader(N=100))
		baseReader.isCacheable = True
		reader1 = CachedReader(baseReader, cache=cache, buildCache=True)
		
		gBase = baseReader.iterateForever()
		g1 = reader1.iterateForever()

		resBase, res1, res2 = [], [], []
		for i in range(len(gBase)):
			resBase.append(next(gBase))
			res1.append(next(g1))
		assert deepCheckEqual(resBase, res1)
		
		# Then, rebuild the cache, it should be dirty, thus a new roll should've been generated
		newBase = RandomIndexReader(baseReader, seed=100)
		newBase.isCacheable = True
		newReader = CachedReader(newBase, cache=cache, buildCache=True)
		gNew = newReader.iterate()
		resNew = []
		for i in range(len(newReader)):
			resNew.append(next(gNew))
		assert not deepCheckEqual(res1, resNew)

def main():
	TestCachedReaderBatched().test_dirty_1()
	# TestCachedReaderBatched().test_iterateOneEpoch_2()
	# TestCachedReader().test_getItem_1()
	# TestCachedReaderBatched().test_constructor_1()

if __name__ == "__main__":
	main()