import sys
import numpy as np
from overrides import overrides
from typing import Tuple, List, Any
from nwdata import RandomIndexReader, MergeBatchedReader, StaticBatchedReader
from nwutils.data_structures import deepCheckEqual
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parents[1]))
from test_reader import DummyDataset
from test_batched_reader import DummyBatchedDataset

def mergeFn(x):
	rgbs = np.stack([y["data"]["rgb"] for y in x], axis=0)
	classes = np.stack([y["labels"]["class"] for y in x], axis=0)
	return {"data" : {"rgb" : rgbs}, "labels" : {"class" : classes}}

class TestRandomIndexReader:
	def test_constructor_1(self):
		reader = RandomIndexReader(DummyDataset(), seed=42)
		assert not reader is None

	def test_iterateForever_1(self):
		items = []
		N = 10
		readers = [RandomIndexReader(DummyDataset(), seed=i) for i in range(N)]
		for i in range(N):
			generator = readers[i].iterate()
			epochItems = []
			for j in range(len(generator)):
				epochItems.append(next(generator))
			items.append(epochItems)

		# len(reader)^10 == 10^10 chance of this test not passing :)
		firstItems = items[0]
		equal = []
		for i in range(1, N):
			ixItems = items[i]
			check = deepCheckEqual(firstItems, ixItems)
			equal.append(check)
		assert sum(equal) != N - 1

class TestRandomIndexBatchedDataset:
	def test_constructor_1(self):
		reader = RandomIndexReader(DummyBatchedDataset(), seed=42)
		assert not reader is None

	def test_iterateForever_1(self):
		N = 10
		readers = [RandomIndexReader(DummyBatchedDataset(), seed=i) for i in range(N)]
		items = []
		for i in range(N):
			generator = readers[i].iterate()
			epochItems = []
			for j in range(len(generator)):
				epochItems.append(next(generator))
			items.append(epochItems)
		# len(reader)^10 == 10^10 chance of this test not passing :)
		firstItems = items[0]
		equal = []
		for i in range(1, N):
			ixItems = items[i]
			try:
				check = deepCheckEqual(firstItems, ixItems)
			except Exception as e:
				print(e)
				breakpoint()
			equal.append(check)
		assert sum(equal) != N - 1

	def test_iterateForever_2(self):
		def f(i):
			reader = DummyDataset()
			reader2 = RandomIndexReader(reader, seed=i)
			reader3 = MergeBatchedReader(reader2, mergeFn=mergeFn)
			reader4 = StaticBatchedReader(reader3, batchSize=10)
			return reader4
		N = 10
		readers = [f(i) for i in range(N)]
		items = []
		for i in range(N):
			epochItems = []
			generator = readers[i].iterate()
			for j in range(len(generator)):
				epochItems.append(next(generator))
			items.append(epochItems)
		# len(reader)^10 == 10^10 chance of this test not passing :)
		firstItems = items[0]
		equal = []
		for i in range(1, N):
			ixItems = items[i]
			try:
				check = deepCheckEqual(firstItems, ixItems)
			except Exception as e:
				print(e)
				breakpoint()
			equal.append(check)
		assert sum(equal) != N - 1

def main():
	TestRandomIndexReader().test_iterateForever_1()

if __name__ == "__main__":
	main()