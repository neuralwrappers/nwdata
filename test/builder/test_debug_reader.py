import sys
from nwdata import DebugReader
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parents[1]))
from test_reader import DummyDataset
from test_batched_reader import DummyBatchedDataset

class TestDebugReader:
	def test_constructor_1(self):
		reader = DebugReader(DummyDataset(), N=3)

	def test_len_1(self):
		reader = DebugReader(DummyDataset(), N=3)
		assert len(reader) == 3

	def test_iterateOneEpoch_1(self):
		reader = DebugReader(DummyDataset(), N=7)
		g = reader.iterateOneEpoch()
		for i, item in enumerate(g):
			pass
		assert i == 6
