import sys
import os
import numpy as np
from nwutils.data_structures import deepCheckEqual
from nwdata import SplitReader, CombinedReader, StaticBatchedReader
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parents[1]))
from test_reader import DummyDataset
from test_batched_reader import DummyBatchedDataset

class TestSplitReader:
	def test_constructor_1(self):
		reader = SplitReader(DummyDataset(), splits={"train" : (0, 0.8), "validation" : [0.8, 1]})
		assert not reader is None

	def test_constructor_2(self):
		reader = SplitReader(DummyDataset(), splits={"train" : (0, 0.8), "validation" : [0.8, 1]})
		assert not reader is None
		trainReader, validationReader = reader[["train", "validation"]]
		assert not trainReader is None
		assert not validationReader is None

	def test_iterate_1(self):
		baseReader = DummyDataset()
		reader = SplitReader(baseReader, splits={"train" : (0, 0.8), "validation" : [0.8, 1]})
		trainReader, validationReader = reader[["train", "validation"]]
		g = baseReader.iterate()
		gTrain = trainReader.iterate()
		gVal = validationReader.iterate()
		assert len(g) == 10 and len(gTrain) == 8 and len(gVal) == 2

		for i in range(len(g)):
			item = next(g)
			itemOther = next(gTrain) if i < len(gTrain) else next(gVal)
			assert deepCheckEqual(item, itemOther), "%d) %s vs %s" % (i, item, itemOther)

	def test_split_combine_1(self):
		reader1 = DummyDataset()
		splitReaders = SplitReader(reader1, splits={"train" : (0, 0.8), "validation" : [0.8, 1]})
		trainReader, validationReader = splitReaders[["train", "validation"]]
		reader2 = CombinedReader([trainReader, validationReader])
		g1 = reader1.iterate()
		g2 = reader2.iterate()
		assert len(g1) == 10 and len(g1) == len(g2)
		for i in range(len(g1)):
			item1 = next(g1)
			item2 = next(g2)
			assert deepCheckEqual(item1, item2), "%d) %s vs %s" % (i, item1, item2)

class TestSplitBatchedDataset:
	def test_constructor_1(self):
		reader = SplitReader(DummyBatchedDataset(), splits={"train" : (0, 0.8), "validation" : [0.8, 1]})
		assert not reader is None

	def test_constructor_2(self):
		reader = SplitReader(DummyBatchedDataset(), splits={"train" : (0, 0.8), "validation" : [0.8, 1]})
		assert not reader is None
		trainReader, validationReader = reader[["train", "validation"]]
		assert not trainReader is None
		assert not validationReader is None

	def test_iterate_1(self):
		baseReader = DummyBatchedDataset()
		reader = SplitReader(baseReader, splits={"train" : (0, 0.75), "validation" : [0.75, 1]})
		trainReader, validationReader = reader[["train", "validation"]]
		g = baseReader.iterate()
		gTrain = trainReader.iterate()
		gVal = validationReader.iterate()
		assert len(g) == 4 and len(gTrain) == 3 and len(gVal) == 1

		for i in range(len(g)):
			item = next(g)
			itemOther = next(gTrain) if i < len(gTrain) else next(gVal)
			assert deepCheckEqual(item, itemOther), "%d) %s vs %s" % (i, item, itemOther)

	def test_split_combine_1(self):
		reader1 = DummyBatchedDataset()
		g1 = reader1.iterate()

		splitReaders = SplitReader(reader1, splits={"train" : (0, 0.8), "validation" : [0.8, 1]})
		trainReader, validationReader = splitReaders[["train", "validation"]]
		gTrain = trainReader.iterate()
		gVal = validationReader.iterate()

		reader2 = CombinedReader([trainReader, validationReader])
		g2 = reader2.iterate()

		assert len(g1) == 4 and len(g1) == len(g2), "%d vs %d" % (len(g1), len(g2))
		for i in range(len(g1)):
			item1 = next(g1)
			item2 = next(g2)
			assert deepCheckEqual(item1, item2), "%d) %s vs %s" % (i, item, item2)

	def test_split_static_batched_1(self):
		reader = DummyBatchedDataset(N=100)
		reader = StaticBatchedReader(reader, 30)
		reader = SplitReader(reader, {"train" : (0, 0.8), "validation" : (0.8, 1)})
		trainReader, validationReader = reader[["train", "validation"]]
		assert len(trainReader[0]["data"]["rgb"]) == 30 and len(trainReader) == 3
		assert len(validationReader[0]["data"]["rgb"]) == 10 and len(validationReader) == 1
		# This should be theoretically doable as we can always rebatch the batched dataset view, but it fails.
		trainReader = StaticBatchedReader(trainReader, 50)
		assert len(trainReader) == 2
		g = trainReader.iterate()
		for i in range(g):
			_ = next(g)

	def test_split_static_batched_1(self):
		reader = DummyBatchedDataset(N=100)
		reader = StaticBatchedReader(reader, 30)
		reader = SplitReader(reader, {"train" : (0, 0.8), "validation" : (0.8, 1)})
		trainReader, validationReader = reader[["train", "validation"]]
		assert len(trainReader[0]["data"]["rgb"]) == 30 and len(trainReader) == 3
		assert len(validationReader[0]["data"]["rgb"]) == 10 and len(validationReader) == 1
		# This should be theoretically doable as we can always rebatch the batched dataset view, but it fails.
		trainReader = StaticBatchedReader(trainReader, 50)
		assert len(trainReader[0]["data"]["rgb"]) == 50 and len(trainReader) == 2
		g = trainReader.iterate()
		for i in range(g):
			_ = next(g)


if __name__ == "__main__":
	TestSplitBatchedDataset().test_split_static_batched_1()
