import sys
import numpy as np
from nwutils.batched import getBatchLens
from nwutils.list import flattenList
from nwdata import CachedReader, MergeBatchedReader, StaticBatchedReader
from simple_caching import DictMemory
from pathlib import Path

sys.path.append(str(Path(__file__).absolute().parents[1]))
from test_reader import DummyDataset

def mergeItems(items):
	rgbs = np.stack([item["data"]["rgb"] for item in items], axis=0)
	classes = len(items) * [0]
	item = {"data": {"rgb" : rgbs}, "labels" : {"class" : classes}}
	return item

class TestComboCompounds:
	def test_combo_1(self):
		reader1 = DummyDataset()
		reader2 = CachedReader(reader1, cache=DictMemory(), buildCache=False)
		reader3 = MergeBatchedReader(reader2, mergeFn=mergeItems)
		reader4 = StaticBatchedReader(reader3, 3)

		g2 = reader2.iterate()
		for i in range(len(g2)):
			assert reader2.cache.check(reader2.cacheKey(i)) == False

		g4 = reader4.iterate()
		for i in range(len(g4)):
			_ = next(g4)

		for i in range(len(g2)):
			k = reader2.cacheKey(i)
			chck = reader2.cache.check(k)
			assert chck == True

	def test_combo_2(self):
		reader1 = DummyDataset()
		reader2 = CachedReader(reader1, cache=DictMemory(), buildCache=True)
		reader3 = MergeBatchedReader(reader2, mergeFn=mergeItems)
		reader4 = StaticBatchedReader(reader3, 3)

		rgbs4 = []
		g4 = reader4.iterate()
		for i in range(len(g4)):
			items = next(g4)
			rgb = items["data"]["rgb"]
			assert len(rgb) == getBatchLens(reader4.batches)[i]
			rgbs4.append(rgb)
		rgbs4 = np.concatenate(rgbs4)

		rgbs2 = []
		g2 = reader2.iterate()
		for i in range(len(g2)):
			assert reader2.cache.check(reader2.cacheKey(i)) == True
			item = next(g2)
			rgbs2.append(item["data"]["rgb"][None])
		rgbs2 = np.concatenate(rgbs2)
		assert np.abs(rgbs2 - rgbs4).sum() <= 1e-5

	def test_combo_3(self):
		# TODO: combo_1 fails with batchesFn.
		batches = [[0], [1], [2], [3, 9, 8], [4], [5], [6], [7]]
		reader1 = DummyDataset()
		reader2 = MergeBatchedReader(reader1, mergeFn=mergeItems, batchesFn=lambda : batches)
		reader3 = StaticBatchedReader(reader2, 3)
		g2 = reader2.iterate()
		g3 = reader3.iterate()

		# Predefined order, but going through all dataset.
		rgbs = []
		assert len(g2) == len(batches)
		for i in range(len(g2)):
			item = next(g2)
			rgb = item["data"]["rgb"]
			rgbs.append(rgb)
			assert rgb.shape[0] == len(batches[i])
		rgbs = np.concatenate(rgbs)

		# Regular order
		assert len(g3) == 4
		rgbs2 = []
		for i in range(len(g3)):
			item = next(g3)
			rgbs2.append(item["data"]["rgb"])
		rgbs2 = np.concatenate(rgbs2)
		rgbs2 = rgbs2[flattenList(batches)]
		assert np.abs(rgbs - rgbs2).sum() <= 1e-5

if __name__ == "__main__":
	TestComboCompounds().test_combo_3()